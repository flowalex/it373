Clarification of folder names,  
===============================

  OstepCH6 are scripts to complete context switch and lmbench, lm bench has 4 scripts due to my lack of expertise with shell scripting  
------------------------------  
 - OstepCH7-9 is all of the scheduling code and scripts  
 - Repeat is the repeat.c code  
 - cpuC is the Ostep into code assignment  
 - ostepPython is the Code and script for the Ostep Chapter 4 assingment  
 - quad is the quadroupler code  
  
The following folders have a script to run the code for the homework assignment listed, but the user still needs to take screenshots and answer any questions that are included with the chapter  
------------------------------
 - OstepCH6: scripts for contextswitch install and compile, as well as lmbench, which is 4 seperate scripts  
 - OstepCH7-9: scripts for those chapters assingments on scheduling  
 - ostepPython: script for the chapter 4 process commands  
