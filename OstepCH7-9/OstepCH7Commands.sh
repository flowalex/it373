#!/bin/sh

# Linux


echo " Copyright (C) <2017>  <Alex Wolf> <github.com/flowalex999>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    This Script Will run the OSTEP commands for Chapter 7 for IT 373 at Depaul University
    "
    
sleep 5s

echo "Each command will wait 30 or 60 Seconds before continuing so that you can see the output and take a screenshot"

sleep 5s

#OSTEP Commands For IT 373

#Chapter 7:

	chmod +x scheduler.py

#	Question 1:
		
echo "Question 1 Commands:"	
		
echo "		./scheduler.py -p FIFO -j 3 -s 200
		./scheduler.py -p SJF -j 3 -s 200  
	"	
	sleep 5

#		Run these after one at a time:
		
		./scheduler.py -p FIFO -j 3 -s 200 -c
		./scheduler.py -p SJF -j 3 -s 200  -c
sleep 60

#	Question 2:
    
 echo "Question 2 Commands: "
        
	echo "	./scheduler.py -p FIFO -j 3 -s 100 "
	echo "	./scheduler.py -p FIFO -j 3 -s 200"
	echo "	./scheduler.py -p FIFO -j 3 -s 300"
	echo "	./scheduler.py -p SJF -j 3 -s 100"
	echo " ./scheduler.py -p SJF -j 3 -s 200"
	echo "	./scheduler.py -p SJF -j 3 -s 300"
		
sleep 5

#		Run these after one at a time:
		
		./scheduler.py -p FIFO -j 3 -s 100 -c
		./scheduler.py -p FIFO -j 3 -s 200 -c
		./scheduler.py -p FIFO -j 3 -s 300 -c
		./scheduler.py -p SJF -j 3 -s 100 -c
		./scheduler.py -p SJF -j 3 -s 200 -c
		./scheduler.py -p SJF -j 3 -s 300 -c

sleep 60
#	Question 3:
        
  
        echo "Question 3 Command:"
        echo "./scheduler.py -p RR -q 1"

		sleep 5

 #       Run these after
        ./scheduler.py -p RR -q 1 -c
	
sleep 60

	echo "Rest of questions are comparing the schedulers"
