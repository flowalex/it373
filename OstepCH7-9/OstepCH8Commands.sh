#!/bin/sh

# Linux


echo " Copyright (C) <2017>  <Alex Wolf> <github.com/flowalex999>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    This Script Will run the OSTEP commands for Chapter 8 for IT 373 at Depaul University
    "
    
sleep 5s

echo "Each command will wait 10 or 60 Seconds before continuing so that you can see the output and take a screenshot"

sleep 5s

#OSTEP Commands For IT 373

	
#Chapter 8:
	
	chmod +x mlfq.py

	#Question 1:
	    
	 #   Run these first one at a time:
	    
	    ./mlfq.py -j 2 -s 2
	    
sleep 10

	  #  Run these after one at a time:
	    
	    echo "Question 1 Command: ./mlfq.py -j 2 -s 2 -c"

sleep 5
	    
	    ./mlfq.py -j 2 -s 2 -c
	 
     sleep 60

   # Question 2:
   echo "Question 2 Command: ./mlfq.py -j 2 -s 2 -c
   "
   sleep 5
   
    	./mlfq.py -j 2 -s 2 -c

	sleep 60
   
    echo "Question 3: This command has not been tested to run, I wrote this as a proof of concept as this is what it should be"
    #Question 3:
    	
    	
	echo "./mlfq.py -p RR -q -c"
    
    
    sleep 60

    #Question 4: 
    echo "Question 4 Command: ./mlfq.py -j 2 -S 4a -S 4b"
     ./mlfq.py -j 2 -S 4a -S 4b
     
    echo "Question 5: This command has not been tested to run, I wrote this as a proof of concept as this is what it should be"
    
    sleep 5
    
    echo 	"./mlfq -p RR -q 10"
    
    sleep 60
    #Question 6:
	    
	        ./mlfq.py -I
	        
            sleep 30
            
        echo "Question 6 Command: ./mlfq.py -I -c"
        sleep 5

            ./mlfq.py -I -c
	        
	    sleep 60
