#!/bin/sh

# Linux


echo " Copyright (C) <2017>  <Alex Wolf> <github.com/flowalex999>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    This Script Will run the OSTEP commands for Chapter 9 for IT 373 at Depaul University
    "
    
sleep 5s

echo "Each command will wait 30 or 60 Seconds before continuing so that you can see the output and take a screenshot"

sleep 5s

#OSTEP Commands For IT 373

	
#Chapter 9:

    chmod +x lottery.py
    
    #Question 1:
    
   

    echo "Question 1 Command: "
    
    echo "./lottery.py -j 3 -s 1 -c
        ./lottery.py -j 3 -s 2 -c
        ./lottery.py -j 3 -s 3 -c
"
sleep 5

        ./lottery.py -j 3 -s 1 -c
        ./lottery.py -j 3 -s 2 -c
        ./lottery.py -j 3 -s 3 -c

       sleep 60 
    
    
    #Question 2:
        
        

echo "Question 2 Command: ./lottery.py -l 10:1,10:100 -c"

sleep 5

        ./lottery.py -l 10:1,10:100 -c
    
    sleep 60

    #Question 3 
        

echo "Question 3 Command: ./lottery.py -l 100:100,100:100 -c "

        ./lottery.py -l 100:100,100:100 -c 
    
    sleep 60

    #Question 4
    
    echo "Question 4 Command:	 ./lottery.py -l 100:100,100:100 -q 10 -c"

    sleep 5

	  ./lottery.py -l 100:100,100:100 -q 10 -c

sleep 60