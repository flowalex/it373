#!/bin/sh

# Linux


echo -e " Copyright (C) <2017>  <Alex Wolf> <github.com/flowalex999>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version. \n

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.\n

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.\n
    
    This Script Will run the commands for the Ostep Chapter 6 assignments for IT 373 at Depaul University \n
    "
    
sleep 5s

cd /usr/lib/lmbench/bin/x86_64-linux-gnu

ls CON*

sleep 10s

cp CON* ~/workspace/



cd ~/workspace/

echo "please edit the config file with the following data:"
echo"
DISKS=\"\"
DISK_DESC=\"\"
OUTPUT=/dev/tty
ENOUGH=1000000
FASTMEM=\"NO\"
FILE=/home/ubuntu/workspace/XXX
FSDIR=/home/ubuntu/workspace
INFO=INFO.jdebettencourt-it373try2-4675169
LINE_SIZE=128
LOOP_O=0.00001861
MAIL=no
TOTAL_MEM=52339
MB=250
MHZ=\"2671 MHz, 0.3744 nanosec clock\"
MOTHERBOARD=\"\"
NETWORKS=\"\"
OS=\"x86_64-linux-gnu\"
PROCESSORS=\"8\"
REMOTE=\"\"
SLOWFS=\"NO\"
SYNC_MAX=\"1\"
LMBENCH_SCHED=\"DEFAULT\"
TIMING_O=0
RSH=rsh
RCP=rcp
VERSION=3.0-20170425
BENCHMARK_HARDWARE=NO
BENCHMARK_OS=YES
BENCHMARK_SYSCALL=
BENCHMARK_SELECT=
BENCHMARK_SIG=
BENCHMARK_PROC=
BENCHMARK_CTX=
BENCHMARK_PAGEFAULT=
BENCHMARK_FILE=
BENCHMARK_MMAP=
BENCHMARK_PIPE=
BENCHMARK_UNIX=
BENCHMARK_UDP=
BENCHMARK_TCP=
BENCHMARK_CONNECT=
BENCHMARK_RPC=
BENCHMARK_HTTP=
BENCHMARK_BCOPY=
BENCHMARK_MEM=
BENCHMARK_OPS=
"

echo
"You can highlight and copy and paste into the file make sure to save it when finished making changes
Please run part 3 to finish running lm bench
"