#!/bin/sh

# Linux


echo -e " Copyright (C) <2017>  <Alex Wolf> <github.com/flowalex999>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version. \n

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.\n

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.\n
    
    This Script Will run the commands for the Ostep Chapter 6 assignments for IT 373 at Depaul University \n
    "
    
sleep 5s



sudo apt-get update

sudo apt-get install lmbench -y

echo "Lm bench will now run, in cloud9 it will fail so after configuring it wait 5 minutes and press control +c (command +c on macs)"
echo -e "my recomendation for configuration is:\n
1\n
1\n
16\n
all\n
no\n
no\n
press enter/return on your keyboard \n
press enter/return on your keyboard \n
then enter the frequency and the decimal points as listed as without the brackets [frequency, .XXXX] \n
then ~/Workspace/ \n
then ~/Workspace/ \n
then it will ask if you want to join the mailing list, I usually say no \n
LM bench will run and fail the first time remember to end it after a few minutes"

sudo lmbench-run
