#bin/sh

#linux


echo -e " Copyright (C) <2017>  <Alex Wolf> <github.com/flowalex999>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version. \n

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.\n

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.\n
    
    This Script Will run the commands for the Ostep Chapter 6 assignments for IT 373 at Depaul University \n
    "
    
sleep 5s

cd ~/workspace/

sudo cp CON* /usr/lib/lmbench/bin/x86_64-linux-gnu

echo -e "LMbench will not run please be paitent it takes at least 5 minutes after configuration for it to complete its run
my recomendation for configuration is:
1
1
16
all
no
no
press enter/return on your keyboard 
press enter/return on your keyboard 
then enter the frequency and the decimal points as listed as without the brackets [frequency, .XXXX]
then ~/Workspace/ 
then ~/Workspace/
then it will ask if you want to join the mailing list, I usually say no

please run lmbench using the command sudo lmbench-run and when it finishes please run part 4 of the script
"


