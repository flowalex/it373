#bin/sh

#linux


echo -e " Copyright (C) <2017>  <Alex Wolf> <github.com/flowalex999>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version. \n

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.\n

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.\n
    
    This Script Will run the commands for the Ostep Chapter 6 assignments for IT 373 at Depaul University \n
    "
    
sleep 5s

cd ~/Workspace/
echo -e " Context Switch was created by github user tsuna\n
https://github.com/tsuna/contextswitch "
git clone https://github.com/tsuna/contextswitch.git

cd contextswitch

gcc -o timectxsw timectxsw.c -pthread -march=native -O3 -mno-avx -D_XOPEN_SOURCE=600 -D_GNU_SOURCE -std=c99 -W -Wall -Werror
gcc -o timectxswws timectxswws.c -pthread -march=native -O3 -mno-avx -D_XOPEN_SOURCE=600 -D_GNU_SOURCE -std=c99 -W -Wall -Werror
gcc -o timetctxsw timetctxsw.c -pthread -march=native -O3 -mno-avx -D_XOPEN_SOURCE=600 -D_GNU_SOURCE -std=c99 -W -Wall -Werror
gcc -o timetctxsw2 timetctxsw2.c -pthread -march=native -O3 -mno-avx -D_XOPEN_SOURCE=600 -D_GNU_SOURCE -std=c99 -W -Wall -Werror
gcc -o timesyscall timesyscall.c -pthread -march=native -O3 -mno-avx -D_XOPEN_SOURCE=600 -D_GNU_SOURCE -std=c99 -W -Wall -Werror

chmod +x cpubench.sh
sudo ./cpubench.sh