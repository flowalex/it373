#bin/sh

#linux

echo " Copyright (C) <2017>  <Alex Wolf> <github.com/flowalex999>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    This Script Will run the OSTEP commands for Chapter 4 for IT 373 at Depaul University
    please remember to take screenshots
    "
    
    sleep 5s
    chmod +x process-run.py
echo "Question 1:
    ./process-run.py -l 5:100,5:100
   "
./process-run.py -l 5:100,5:100

sleep 15

echo "Question 2:
    ./process-run.py  -l 4:100,1:0
   "
./process-run.py  -l 4:100,1:0

sleep 15

echo "Question 3:
    ./process-run.py -l 1:0,4:100
   "
./process-run.py -l 1:0,4:100

sleep 15

echo "Question 4:
    ./process-run.py -l 1:0,4:100 -S SWITCH_ON_END
   "
./process-run.py -l 1:0,4:100 -S SWITCH_ON_END

sleep 15

echo "Question 5:
    ./process-run.py -l 1:0,4:100 -S SWITCH_ON_IO
   "
./process-run.py -l 1:0,4:100 -S SWITCH_ON_IO

sleep 15

echo "Question 5:
    ./process-run.py -l 3:0,5:100,5:100,5:100 -S SWITCH_ON_IO -I IO_RUN_LATER
   "
./process-run.py -l 3:0,5:100,5:100,5:100 -S SWITCH_ON_IO -I IO_RUN_LATER

sleep 15

echo "Question 5:
    ./process-run.py -l 3:0,5:100,5:100,5:100 -S SWITCH_ON_IO -I IO_RUN_IMMEDIATE
   "
./process-run.py -l 3:0,5:100,5:100,5:100 -S SWITCH_ON_IO -I IO_RUN_IMMEDIATE

sleep 15

echo "Question 5:
    ./process-run.py -l 3:0,5:100,5:100,5:100 -S SWITCH_ON_IO -I IO_RUN_IMMEDIATE
   "
./process-run.py -l 3:0,5:100,5:100,5:100 -S SWITCH_ON_IO -I IO_RUN_IMMEDIATE

sleep 15

echo "Question 5:
    ./process-run.py -s 1 -l 3:50,3:50, -s 2 -l 3:50,3:50, -s 3 -l 3:50,3:50. -S SWITCH_ON_IO -I IO_RUN_LATER
   "
./process-run.py -s 1 -l 3:50,3:50, -s 2 -l 3:50,3:50, -s 3 -l 3:50,3:50. -S SWITCH_ON_IO -I IO_RUN_LATER

echo "Question 5:
    ./process-run.py -s 1 -l 3:50,3:50, -s 2 -l 3:50,3:50, -s 3 -l 3:50,3:50. -S SWITCH_ON_IO -I IO_RUN_IMMEDIATE
   "
./process-run.py -s 1 -l 3:50,3:50, -s 2 -l 3:50,3:50, -s 3 -l 3:50,3:50. -S SWITCH_ON_IO -I IO_RUN_IMMEDIATE

sleep 15

echo "Question 5:
    ./process-run.py -s 1 -l 3:50,3:50, -s 2 -l 3:50,3:50, -s 3 -l 3:50,3:50. -S SWITCH_ON_END -I IO_RUN_LATER
   "
./process-run.py -s 1 -l 3:50,3:50, -s 2 -l 3:50,3:50, -s 3 -l 3:50,3:50. -S SWITCH_ON_END -I IO_RUN_LATER

echo "Question 5:
    ./process-run.py -s 1 -l 3:50,3:50, -s 2 -l 3:50,3:50, -s 3 -l 3:50,3:50. -S SWITCH_ON_END -I IO_RUN_IMMEDIATE
   "
./process-run.py -s 1 -l 3:50,3:50, -s 2 -l 3:50,3:50, -s 3 -l 3:50,3:50. -S SWITCH_ON_END -I IO_RUN_IMMEDIATE


